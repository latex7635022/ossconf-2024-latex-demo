# Demo vytvárania *LaTeX* dokumentov v prostredí *org-mode* v GNU Emacs

Súbor `demo.org` v adresári `demo` je zdrojovým súborom vytvoreným v *org-mode*. Ostatné súbory (.pdf, .tex) sú vyexportované prostredníctvom funkcie `org-latex-export-to-pdf`.

Kvôli balíku `hwemoji` export funguje pod *pdflatex*, pri používaní napr. *lualatex* je potrebné iné rozšírenie na generovanie emoji. Alebo to treba jednoducho zmazať 😉.

Happy hacking!